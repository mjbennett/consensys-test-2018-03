package org.strukt;

/**
 * Created by micha on 15/03/2018.
 * Copyright 2016 GB Associates. All Rights Reserved.
 */
public class Dag {
    private final Node primaryNode;

    public Dag(Node primaryNode) {
        this.primaryNode = primaryNode;
    }

    public Node getPrimaryNode() {
        return primaryNode;
    }
}
