package org.strukt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.Boolean.TRUE;

/**
 * Created by micha on 15/03/2018.
 * Copyright 2016 GB Associates. All Rights Reserved.
 *
 * class to visit the Dag and give us some stats on it.
 */
public class DagVisitor {

    public static final Logger LOG = LoggerFactory.getLogger(DagVisitor.class);

    private final Dag dag;


    public DagVisitor(Dag dag) {
        this.dag = dag;
    }

    public int visit() {
        LOG.debug("Visiting DAG: {}", dag.getPrimaryNode());
        Set<String> visitedNodeSet = visitorAcc(dag.getPrimaryNode(), new HashSet<>());
        return visitedNodeSet.size();
    }

    private Set<String> visitorAcc(Node node, Set<String> visitedNodeMap) {
        LOG.debug("Visiting node: {} Map: {}", node, visitedNodeMap);
        if(!visitedNodeMap.contains(node.getName())){
            // visit node
            visitedNodeMap.add(node.getName());
            LOG.debug("First time visiting node: {} Map {}", node.getName(), visitedNodeMap);
            for (Node child : node.getChildren()) {
                visitedNodeMap.addAll(visitorAcc(child, visitedNodeMap));
            }
            if(node.getChildren().isEmpty()){
                LOG.debug("End of DAG for node: {} returning: {}" + node.getName(), visitedNodeMap);
            }
            return visitedNodeMap;
        } else {
            LOG.debug("Skipping node: {} map: {}", node.getName(), visitedNodeMap.size());
            return visitedNodeMap;
        }
    }

}
