package org.strukt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by micha on 15/03/2018.
 * Copyright 2016 GB Associates. All Rights Reserved.
 */
public class Node {
    private final String name;
    private List<Node> children = new ArrayList<>();

    public Node(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Node> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;
        if (!name.equals(node.name)) return false;
        return children.equals(node.children);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + children.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                ", children=" + children +
                '}';
    }

    /**
     * Inner helper builder for tests
     */
    public static final class NodeBuilder {
        private String name;
        private List<Node> children = new ArrayList<>();

        private NodeBuilder() {
        }

        public static NodeBuilder aNode() {
            return new NodeBuilder();
        }

        public NodeBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public NodeBuilder withChildren(Node... nodes) {
            this.children = Arrays.asList(nodes);
            return this;
        }

        public Node build() {
            Node node = new Node(name);
            node.children = this.children;
            return node;
        }
    }
}
