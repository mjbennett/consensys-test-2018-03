import org.junit.Before;
import org.junit.Test;
import org.slf4j.impl.SimpleLogger;
import org.strukt.Dag;
import org.strukt.DagVisitor;
import org.strukt.Node;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by micha on 15/03/2018.
 * Copyright 2016 GB Associates. All Rights Reserved.
 */
public class DagTest {

    @Before
    public void setUp() throws Exception {
        // disable this to remove logging
        System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "DEBUG");
    }

    @Test
    public void givenDagNodesEnsureTheyAreConfiguredCorrectly() throws Exception {
        Dag dag = buildDag();

        assertThat(dag.getPrimaryNode().getName(), is("1"));
        List<Node> children = dag.getPrimaryNode().getChildren();
        assertThat(children.size(), is(2));

        Node childOf1_1 = children.get(0);
        assertNode(childOf1_1, "2", 2, "3", "7");
        // 2 points to 3 & 7
        // this is 3 -> end of the line
        Node childOf1_1_1 = childOf1_1.getChildren().get(0);
        assertNode(childOf1_1_1, "3", 0);
        // this is node 7 -> goes to 8
        Node childOf1_1_2 = childOf1_1.getChildren().get(1);
        assertNode(childOf1_1_2, "7", 1, "8");
        // now its child 8 which is end of the road
        Node childOf1_1_2_1 = childOf1_1_2.getChildren().get(0);
        assertNode(childOf1_1_2_1, "8", 0);

        // node 4 -> 5,6,9
        Node childOf1_2 = children.get(1);
        assertNode(childOf1_2, "4", 3, "5", "6", "9");
        // node 5 -> 8,9
        Node childOf1_2_1 = childOf1_2.getChildren().get(0);
        assertNode(childOf1_2_1, "5", 2, "8", "9");

        // node 6 -> 7
        Node childOf1_2_2 = childOf1_2.getChildren().get(1);
        assertNode(childOf1_2_2, "6", 1, "7");
        // node 7 -> 8
        Node childOf1_2_2_1 = childOf1_2_2.getChildren().get(0);
        assertNode(childOf1_2_2_1, "7", 1, "8");
        // node 8 -> EOL
        Node childOf1_2_2_1_1 = childOf1_2_2_1.getChildren().get(0);
        assertNode(childOf1_2_2_1_1, "8", 0);

        // node 9 -> EOL
        Node childOf1_2_3 = childOf1_2.getChildren().get(2);
        assertNode(childOf1_2_3, "9", 0);

    }

    @Test
    public void givenTreeRecurseThroughNodesSuccessfully() throws Exception {
        Dag dag = buildDag();

        // need to visit the dag and assert no of visited nodes is 9
        DagVisitor visitor = new DagVisitor(dag);

        int visitedNodes = visitor.visit();
        assertThat("Visitor didn't return successfully.", visitedNodes, is(9));

    }

    /**
     * Test method to build a DAG for consensys-test
     *
     * @return
     */
    private static Dag buildDag() {

        /*
        Build up node tree
        (1) -> (2,4)
            (2) -> (3,7)
                (3) -> (None)
                    (4) -> (5,6,9)
                        (5) -> (8,9)
                            (6) -> (7)
                                (7) -> (8)
                                    (8) -> (None)
                                        (9) - (None)

            first traversal - 1,2,3,7,8,4,6,9,

         */

        Node no9 = Node.NodeBuilder.aNode().withName("9").build(); // no children
        Node no8 = Node.NodeBuilder.aNode().withName("8").build(); // no children
        Node no7 = Node.NodeBuilder.aNode()
                .withName("7")
                .withChildren(no8)
                .build();
        Node no6 = Node.NodeBuilder.aNode()
                .withName("6")
                .withChildren(no7)
                .build();
        Node no5 = Node.NodeBuilder.aNode()
                .withName("5")
                .withChildren(no8, no9)
                .build();
        Node no4 = Node.NodeBuilder.aNode()
                .withName("4")
                .withChildren(no5, no6, no9)
                .build();
        Node no3 = Node.NodeBuilder.aNode().withName("3").build(); // no children
        Node no2 = Node.NodeBuilder.aNode()
                .withName("2")
                .withChildren(no3, no7)
                .build();
        Node no1 = Node.NodeBuilder.aNode()
                .withName("1")
                .withChildren(no2, no4)
                .build();


        return new Dag(no1);
    }

    private void assertNode(Node node, String name, int childrenSize, String... childrenNames) {
        assertThat(node.getName(), is(name));
        assertThat(node.getChildren().size(), is(childrenSize));
        if (childrenNames != null) {
            for (int i = 0; i < childrenNames.length; i++) {
                assertThat(node.getChildren().get(i).getName(), is(childrenNames[i]));
            }
        }
    }
}
